package com.itau.hellospring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.hellospring.model.Produto;
import com.itau.hellospring.repository.ProdutoRepository;

@Controller
public class ProdutoController {
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	@RequestMapping(path="/produto", method=RequestMethod.POST)
	@ResponseBody
	public Produto inserirProduto(@RequestBody Produto produto) {
		return produtoRepository.save(produto);
	}
	
	@RequestMapping(path="/produto", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Produto> getProdutos() {		
		return produtoRepository.findAll();
	}
	
}
