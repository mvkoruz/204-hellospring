package com.itau.hellospring.controller;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.hellospring.model.Produto;
import com.itau.hellospring.model.Venda;
import com.itau.hellospring.repository.ProdutoRepository;
import com.itau.hellospring.repository.VendaRepository;

@Controller
public class VendaController {

	@Autowired
	VendaRepository vendaRepository;
	
	@Autowired
	ProdutoRepository produtoRepository;

	@RequestMapping(path="/venda", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> inserirVenda(@RequestBody Set<Produto> produtos) {
		Venda venda = new Venda();
		Date DataHj = new Date();
		int qtdTotal = 0; Double total = 0.00;
		for(Produto produto : produtos)
		{
			int qtd = 0;
			qtdTotal  += produto.getQuantidade();
			total+= produto.getQuantidade() * produto.getPreco();
			
			Optional<Produto> produtoQuery = produtoRepository.findById(produto.getId());
			Produto produtoNew = produtoQuery.get();
			qtd = produtoNew.getQuantidade() - produto.getQuantidade();
			produtoNew.setQuantidade(qtd);
			if(qtd < 0) {
				return ResponseEntity.badRequest().body("Quantidade excedeu o estoque.");
			}
			produtoRepository.save(produtoNew);
		}
		venda.setProduto(produtos);
		venda.setQuantidade(qtdTotal);
		venda.setData(DataHj);
		venda.setTotal(total);
		
		vendaRepository.save(venda);
		
		return ResponseEntity.ok(venda);
	}

	@RequestMapping(path="/vendas", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Venda> getVendas() {		
		return vendaRepository.findAll();
	}
}
