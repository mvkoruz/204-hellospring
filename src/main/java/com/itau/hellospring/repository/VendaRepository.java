package com.itau.hellospring.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.hellospring.model.Venda;

public interface VendaRepository extends CrudRepository<Venda, Long> {

}
