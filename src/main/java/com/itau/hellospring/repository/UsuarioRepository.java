package com.itau.hellospring.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.hellospring.model.Usuario;

public interface UsuarioRepository  extends CrudRepository<Usuario, Long>{
	public Optional<Usuario> findByEmail(String email);
}

