package com.itau.hellospring.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.hellospring.model.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Long> {

}
